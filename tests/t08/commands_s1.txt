table_add check_vibrant vibrant_present 1 =>
table_add check_vibrant vibrant_not_present 0 =>
table_add dmac broadcast 0xFFFFFFFFFFFF => 5
table_add dmac local 0x000400000000 => 1
table_add dmac2 local 0x000400000000 => 1
table_add dmac not_local 0x000400000002 => 3
table_add dmac2 not_local 0x000400000002 => 3
table_set_default strip_vibrant a_strip_vibrant
table_add dmac2 broadcast 0xFFFFFFFFFFFF => 5
table_add add_vibrant a_add_vibrant 1 =>
table_add add_vibrant _no_op 0 =>
