#!/usr/bin/python

import dpkt
from dpkt.compat import compat_ord
import socket, struct
import argparse
import sys

import code
from inspect import currentframe, getframeinfo

def debug():
  """ Break and enter interactive method after printing location info """
  # written before I knew about the pdb module
  caller = currentframe().f_back
  method_name = caller.f_code.co_name
  line_no = getframeinfo(caller).lineno
  print(method_name + ": line " + str(line_no))
  code.interact(local=dict(globals(), **caller.f_locals))

def mac_addr(address):
  return ''.join('%02x' % compat_ord(b) for b in address)

def inet_to_str(inet):
  return socket.inet_ntop(socket.AF_INET, inet)

def gen_address_timeseries(pcap, outmacpath, outippath):
  counter = 0
  first = True
  ts_init = 0
  pps_counter_0_1 = 0
  pps_counter_20_21 = 0
  pps_counter_44_45 = 0
  pps_counter_60_61 = 0

  with open(outmacpath, 'w') as outmac, open(outippath, 'w') as outip:

    outmac.write('x, y\n')
    outip.write('x, y\n')

    for ts, buf in pcap:

      if first:
        ts_init = ts
        first = False

      counter += 1
      ts_shifted = ts - ts_init

      if ts_shifted < 1:
        pps_counter_0_1 += 1
      elif ts_shifted >= 20 and ts_shifted < 21:
        pps_counter_20_21 += 1
      elif ts_shifted >= 44 and ts_shifted < 45:
        pps_counter_44_45 += 1
      elif ts_shifted >= 60 and ts_shifted < 61:
        pps_counter_60_61 += 1

      if counter % 10000 == 0:
        sys.stdout.write('.')
        sys.stdout.flush()

      eth = dpkt.ethernet.Ethernet(buf)
      dst_str = mac_addr(eth.dst)
      src_str = mac_addr(eth.src)
      outmac.write(str(ts_shifted) + ', ' + str(int(dst_str, 16)) + '\n')
      outmac.write(str(ts_shifted) + ', ' + str(int(src_str, 16)) + '\n')

      if isinstance(eth.data, dpkt.ip.IP):
        ip = eth.data
        ip_dst_str = struct.unpack("!L", ip.dst)[0]
        ip_src_str = struct.unpack("!L", ip.src)[0]
        outip.write(str(ts_shifted) + ', ' + str(ip_dst_str) + '\n')
        outip.write(str(ts_shifted) + ', ' + str(ip_src_str) + '\n')

  print(str(counter) + ' packets processed')
  print(str(pps_counter_0_1) + ' pps 0-1s')
  print(str(pps_counter_20_21) + ' pps 20-21s')
  print(str(pps_counter_44_45) + ' pps 44-45s')
  print(str(pps_counter_60_61) + ' pps 60-61s')

def parse_args(args):
  parser = argparse.ArgumentParser(description='Analyze addresses in pcap')
  parser.add_argument('--pcap', help='Path for pcap',
                      type=str, action="store")
  parser.add_argument('--outmac', help='Path for MAC address output',
                      type=str, action="store")
  parser.add_argument('--outip', help='Path for IP address output',
                      type=str, action="store")

  return parser.parse_args(args)

def main():
  args = parse_args(sys.argv[1:])
  with open(args.pcap, 'r') as f:
    pcap = dpkt.pcap.Reader(f)
    gen_address_timeseries(pcap, args.outmac, args.outip)

if __name__ == '__main__':
  main()
