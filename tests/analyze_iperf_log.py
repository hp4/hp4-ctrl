#!/usr/bin/python

import argparse
import sys

import code
from inspect import currentframe, getframeinfo

MBPS_COLUMN = 6

def debug():
  """ Break and enter interactive method after printing location info """
  # written before I knew about the pdb module
  caller = currentframe().f_back
  method_name = caller.f_code.co_name
  line_no = getframeinfo(caller).lineno
  print(method_name + ": line " + str(line_no))
  code.interact(local=dict(globals(), **caller.f_locals))

def gen_bandwidth_timeseries(f, outpath):
  time_adjust = 2
  if f.name.split('t09/t09')[1].split('iperf')[0] == 'h1':
    time_adjust = 2
  elif f.name.split('t09/t09')[1].split('iperf')[0] == 'h5':
    time_adjust = 4
  else:
    debug()
  with open(outpath, 'w') as out:
    out.write('x, y\n')
    lines = f.readlines()[3:]
    prev_x = 0.0
    for line in lines:
      if '- - - - -' in line:
        break
      x = line.split('-')[1].split()[0]
      if float(x) - prev_x < 1.0:
        break
      prev_x = float(x)
      x = str(float(x) + time_adjust)
      y = line.split()[MBPS_COLUMN]
      out.write(x + ', ' + y + '\n')

def parse_args(args):
  parser = argparse.ArgumentParser(description='Analyze iperf log')
  parser.add_argument('--iperflog', help='Path for iperflog',
                      type=str, action="store")
  parser.add_argument('--out', help='Path for output',
                      type=str, action="store")

  return parser.parse_args(args)

def main():
  args = parse_args(sys.argv[1:])
  with open(args.iperflog, 'r') as f:
    gen_bandwidth_timeseries(f, args.out)

if __name__ == '__main__':
  main()
